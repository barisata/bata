---
layout: page
title: Publications
---
---

###International Conference Proceedings

* Baris Ata, Ramazan Coban, “Artificial Bee Colony Algorithm Based Linear Quadratic Optimal Controller Design for a Nonlinear Inverted Pendulum”, International Conference on
Advanced Technology &Sciences (ICAT'14), 12-15 August, 2014, Antalya, Turkey, pp. 47-52

* Baris Ata, Ramazan Coban, “Linear Quadratic Optimal Control of an Inverted Pendulum Using the Artificial Bee Colony Algorithm”, 2014 IEEE International Conference on Automation, Quality and Testing, Robotics, 22-24 May, 2014, Cluj-Napoca, Romania, pp.
