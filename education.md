---
layout: page
title: Education
---
---

###PhD
* Cukurova University, Department of Computer Engineering (2014-)

###MS
* Cukurova University, Department of Computer Engineering (2012-2014)

###BS
* Kocaeli University, Department of Computer Engineering (2005-2010) 